## DragonISS4PSU

Dragon 32/64 power supply board clone created using KiCad.

This is a reproduction of the ISS 4 power supply board used in the PAL Dragon machines i.e. the version with the UM1287 TV modulator.

This design incorporates (as LINKB) the flying wire modification seen on some boards.

I have put an extra ground plane on top of the board which can be omitted to make the board single layer for minimum cost.


### Included files

KiCad schematic and board layout files with all footprints used and non-standard library 3d models.

Gerbers and drills can be found in the plots folder and a pdf schematic and rendered images of the board are in the misc folder.


### Practical issues

This design was more about capturing the look of the original board rather than making a modern replacement. All of the components are obtainable with the exception of the custom heatsink. Similar heatsinks are available and could be modified, otherwise some aluminium channel or angle would suffice.

To serve better as a modern replacement, the design should be changed to use a modern +5V 3A regulator in place of the LM309 and standard heatsinks used for the +5V and +12V regulators. A switching regulator for the 5V supply is another possibility.

The connector that feeds the main board could be assembled from Molex KK 396 series parts. e.g. a 9 way 6442 series housing with pre-crimped leads.

The board should be populated to suit the target machine: Dragon 32 machines require a -5V regulator (7905) for REG3 and a 180 Ohm resistor for R5. Dragon 64 machines require a -12V regulator (7912) for REG3 and a wire link in the position of R5.

REG3 should not be attached to the heatsink. Apart from not requiring a heatsink its mounting tab is not 0V potential meaning things will not be happy!


### Disclaimer

I have not had this made so be sure to double-check the circuit, component values and critical dimensions before doing anything expensive e.g. alignment of the switch, connectors and mounting holes.
